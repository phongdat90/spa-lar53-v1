<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/admin/css/app.css">
    <script>
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()])?>;
    </script>
</head>
<body>
<div id="app">

</div>
<script src="/admin/js/app.js"></script>
</body>
</html>