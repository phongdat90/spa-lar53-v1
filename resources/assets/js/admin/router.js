import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
    linkExactActiveClass: 'active',
    routes: [
        {path: '/admin/customer', component: require('./views/customer/index.vue')},
        {path: '/admin/customer/create', component: require('./views/customer/form.vue')},
        {path: '/admin/customer/:id', component: require('./views/customer/show.vue')},
        {path: '/admin/customer/:id/edit', component: require('./views/customer/form.vue'), meta: {model: 'edit'}},

        {path: '/admin/modal/index', component: require('./views/modal/index.vue')}
    ]
});
export default router